import React, {useState, useEffect} from 'react';

function AllSalesList() {
    const [sales, setSales] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8090/api/sales/");

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
            console.log(data)
        }
    }
    useEffect(()=> {
        getData();
    }, [])


    return (
        <table>
            <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Employee ID</th>
                    <th>Purchaser</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map(sale => {
                    return (
                        <tr key={ sale.id }>
                            <td>{ sale.salesperson.first_name }</td>
                            <td>{ sale.salesperson.employee_id }</td>
                            <td>{ sale.customer.first_name }</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>{ sale.price }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default AllSalesList;
