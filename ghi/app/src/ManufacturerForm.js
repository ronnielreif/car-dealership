import React, { useState } from 'react';


function ManufacturerForm () {
    const [formData, setFormData] = useState({
        name: '',
    });

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/manufacturers/';
        try{
            const fetchOptions = {
                method: 'POST',
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            const res = await fetch(url, fetchOptions);
            if (res.ok) {
                setFormData({
                    name: '',
                });
            window.location.reload();
            }
        } catch(e) {
            console.log('An error has occured when POSTing a manufacturer', e);
        }
    }

  const handleFormChange = function({ target }) {
    const { value, name } = target;

    setFormData({
        ...formData,
        [name]: value
    });
  }

  const {
    name,
  } = formData;

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a Manufacturer</h1>
                <form onSubmit={handleSubmit}>

                    <div className="form-floating mb-3">
                    <input value={name} onChange={handleFormChange} type="text" className="form-control" name="name"/>
                    <label htmlFor="name"className="form-label">Name</label>
                    </div>

                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
  )
}


export default ManufacturerForm;
