import React, {useState, useEffect} from 'react';

function SaleForm() {
    const [automobiles, setAutomobile] = useState([])
    const [salesperson, setSalesperson] = useState([])
    const [customer, setCustomer] = useState([])
    const [FormData, setFormData] = useState({
        automobile: "",
        salesperson: "",
        customer: "",
        price: "",
    })

    const getData = async () => {
        const autosurl = "http://localhost:8100/api/automobiles/"
        const autosresponse = await fetch(autosurl);

        if (autosresponse.ok) {
            const data = await autosresponse.json();
            setAutomobile(data.autos)
        }
        const salespeopleUrl = "http://localhost:8090/api/salespeople/"
        const salespeopleresponse = await fetch(salespeopleUrl);

        if (salespeopleresponse.ok) {
            const data = await salespeopleresponse.json();
            setSalesperson(data.salesperson)
        }
        const customerurl = "http://localhost:8090/api/customers/"
        const customerresponse = await fetch(customerurl);

        if (customerresponse.ok) {
            const data = await customerresponse.json();
            setCustomer(data.customer)
        }
    }
    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const salesUrl = "http://localhost:8090/api/sales/"

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(FormData),
            headers: {
                "Content-Type": "application/json",
            },
        }; console.log(fetchConfig)

        const response = await fetch(salesUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                automobile: "",
                salesperson: "",
                customer: "",
                price: "",
            });
        }

        const autosurl = "http://localhost:8100/api/automobiles/"+FormData.automobile+"/"

        const updateConfig = {
            method: "PUT",
            body: JSON.stringify({
                sold: true,
            }),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const autoresponse = await fetch(autosurl, updateConfig);
            console.log(autoresponse)
        if (autoresponse.ok) {
            setFormData({
                sold: "true",
            })
        }

    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...FormData,
            [inputName]: value,
        });
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new sale</h1>
              <form onSubmit={handleSubmit} id="create-sale-form">
                <div className="form-floating mb-3">
                  <select onChange={handleFormChange} value={FormData.automobile} required name="automobile" id="automobile" className="form-select">
                    <option value="">Choose an automobile</option>
                    {automobiles.filter(car => car.sold === false).map(filteredCar => {
                      return (
                        <option key={filteredCar.id} value={filteredCar.import_href}>{filteredCar.vin}</option>
                      )
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select onChange={handleFormChange} value={FormData.salesperson} required name="salesperson" id="salesperson" className="form-select">
                    <option value="">Choose a salesperson</option>
                    {salesperson.map(sales => {
                      return (
                        <option key={sales.id} value={sales.id}>{sales.first_name} {sales.last_name}</option>
                      )
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select onChange={handleFormChange} value={FormData.customer} required name="customer" id="customer" className="form-select">
                    <option value="">Choose a customer</option>
                    {customer.map(custom => {
                      return (
                        <option key={custom.id} value={custom.id}>{custom.first_name} {custom.last_name}</option>
                      )
                    })}
                  </select>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                  <label htmlFor="starts">Price</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}
export default SaleForm;
