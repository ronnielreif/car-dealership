from django.urls import path

from .views import (
    api_list_technicians,
    api_list_appointments,
)

urlpatterns = [
    path("api/technicians/", api_list_technicians, name="api_list_technicians"),
    path("api/appointments/", api_list_appointments, name="api_list_appointments"),
    path('api/appointments/<str:vin>/', api_list_appointments, name='api_list_appointments'),
]
